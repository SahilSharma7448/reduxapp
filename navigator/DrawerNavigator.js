import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {ContactStackNavigator} from './StackNavigator';
import TabNavigator from './TabNavigator';
import SignIn from '../app/screens/AuthScreens/SignIn';
import DrawerBg from '../app/Components/DrawerBg';
 
 
const Drawer = createDrawerNavigator();
 
const DrawerNavigator = () => {
 return (
   <Drawer.Navigator
   drawerContent={DrawerBg}
     // drawerContentOptions={{
     //   // activeTintColor: 'green',
     //   itemStyle: {marginVertical: 5},
     // }}
     >
     <Drawer.Screen
       name="Home"
       options={{drawerLabel: 'First page Option'}}
       component={TabNavigator}
     />
     {/* <Drawer.Screen
       name="SecondPage"
       options={{drawerLabel: 'Second page Option'}}
       component={TabNavigator}
     />
      <Drawer.Screen
       name="SignIn"
       component={SignIn}
       options={{ drawerLabel: 'Home' }}
     /> */}
   
   </Drawer.Navigator>
 );
};
 
export default DrawerNavigator;
 
