import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import SignIn from '../app/screens/AuthScreens/SignIn';
import DrawerScreen from '../navigator/DrawerNavigator';

import Home from '../app/screens/AuthScreens/Home';
import SignUp from '../app/screens/AuthScreens/SignUp';
import Splash from '../app/screens/AuthScreens/Splash';

const Stack = createStackNavigator();
const MainStackNavigator = () => {
  return (
    <Stack.Navigator>


 <Stack.Screen
        name="Splash"
        component={Splash}
        options={{ headerShown: false }}
      />
     
     <Stack.Screen
        name="SignIn"
        component={SignIn}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="DrawerScreen"
        component={DrawerScreen}
        options={{ headerShown: false }}
      />
      
  <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={{ headerShown: false }}
      />
    <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }} />
    </Stack.Navigator>
  );
};

export { MainStackNavigator };
