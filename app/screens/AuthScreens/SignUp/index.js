import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Alert,
  StyleSheet,
} from 'react-native';
import PATH from '../../../Constants/ImagePath';
import * as Utility from '../../../utility/index'
import TextInput from '../../../Constants/TextField';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../utility/index";
import {useDispatch, useSelector} from 'react-redux';
import {loginData, UserList, AdminList,create} from '../../../Redux/User/Action'
function SignUp({navigation}) {
  
const[firstName,setName]=useState('')
const[email,setEmail]=useState('');
const[password,setPassword]=useState('');

const dispatch = useDispatch();
 
const Register =async()=>{

    if (Utility.isFieldEmpty(email && password && firstName )) {
        Alert.alert("", "Please enter all the fields")
      }
  
      else if (Utility.isValidEmail(email)) {
        Alert.alert("", "Please Enter Valid Email")
  
      }
      else {
        let body={
              firstName:firstName.toLocaleLowerCase(),
              //  lastName:LName.toLocaleLowerCase(),
               email:email.toLocaleLowerCase() ,
               password:password.toLocaleLowerCase(),
               role: "customer",
              }
           await   console.log("body",body)

///============ CREATE USER POST API=============

let res= await dispatch(CreateUser(body))
 if(res===true){

  Alert.alert(
   "",
   "User Register Sucessfull",
   [
       { text: "Ok", onPress: () =>goToSignIn() },
   ],
   { cancelable: false },
);
return true;
} 

           }
   }
const   goToSignIn=()=>{
    setLoader(false)
       navigation.dispatch(
           StackActions.reset({
               index: 0,
               actions: [NavigationActions.navigate({ routeName: "SignIn" })]
           })
       ),
           navigation.navigate("SignIn")
   }
  return (
    <View>
      <ImageBackground
        source={PATH.backGroundImage}
        style={{height: '100%', width: '100%'}}>
        
        <Image source={PATH.logo}   resizeMode="contain"     style={styles.logo} />
        <TextInput
          placeHolder={'Name'}
          onChange={(firstName)=>setName(firstName)}
          ></TextInput>
        <TextInput
          placeHolder={'Email'}
          onChange={(email)=>setEmail(email)}
          ></TextInput>
        <TextInput
          placeHolder={'Password'}
          onChange={(password)=>setPassword(password)}
          ></TextInput>
       
        <TouchableOpacity onPress={() => Register()}>
          <View style={styles.buttonView}>
            <Text style={styles.submitText}>Register</Text>
          </View>
        </TouchableOpacity>

        
      </ImageBackground>
     
    </View>
  );
}
export default SignUp;

const styles = StyleSheet.create({
  logo: {
    height:hp("30%"),
    width: wp("20%"),
    alignSelf: 'center',
    marginTop: '7%',
  },
  emailInput: {
    marginLeft: '5%',
    color: 'white',
  },

  textInputView: {
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    width: '80%',
    flexDirection: 'row',
    marginLeft: '10%',
  },
  passwordView: {
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    width: '80%',
    flexDirection: 'row',
    marginLeft: '10%',
    marginTop: '10%',
  },
  emailSize: {
    height: 25,
    width: 25,
    marginTop: '3%',
  },
  forgotText: {
    color: 'white',
    marginLeft: '55%',
    marginTop: '5%',
  },
  buttonView: {
    height: 40,
    width: '80%',
    backgroundColor: 'white',
    marginLeft: '10%',
    marginTop: '7%',
    borderRadius: 30,
    justifyContent: 'center',
  },
  submitText: {
    color: 'green',
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  facebookView: {
    height: '6%',
    width: '80%',
    backgroundColor: '#03fcba',
    marginLeft: '10%',
    marginTop: '3%',
    borderRadius: 30,
    justifyContent: 'center',
  },
});
