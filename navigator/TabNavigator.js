import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Alert, Image, StyleSheet} from 'react-native';
import {MainStackNavigator, ContactStackNavigator} from './StackNavigator';
import Home from '../app/screens/AuthScreens/Home';
// import AdminHome from '../app/screens/Home';
// import MyProfile from '../app/screens/MyProfile';
import PATH from '../app/Constants/ImagePath';
import Tabs from "../app/Components/Tabbar"
const Tab = createBottomTabNavigator();
const BottomTabNavigator = () => {
 return (
   <Tab.Navigator>
     <Tab.Screen
       name="APP USER"
       component={Tabs}
       options={({route}) => ({
         tabBarIcon: ({focused}) => (
           <Image
             source={PATH.logo}
             style={focused ? styles.selectedTabImg : styles.tabImg}
           />
         ),
       })}
     />
  
   </Tab.Navigator>
 );
};
 
export default BottomTabNavigator;
const styles = StyleSheet.create({
 tabImg: {
   width: 30,
   height: 30,
   alignSelf: 'center',
   tintColor: 'grey',
 },
 selectedTabImg: {
   width: 30,
   height: 30,
   alignSelf: 'center',
   tintColor: 'skyblue',
 },
});
 
 
