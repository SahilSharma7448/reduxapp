import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Alert,
  StyleSheet,
} from 'react-native';
import PATH from '../../../Constants/ImagePath';
import {useDispatch, useSelector} from 'react-redux';
import {loginData, UserList, AdminList} from '../../../Redux/User/Action';

import TextInput from '../../../Constants/TextField';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../utility/index";

function SignIn({navigation}) {
  

const[email,setEmail]=useState('');
const[password,setPassword]=useState('');
const dispatch = useDispatch();

//   const login = async () => {
//     if (email == '' && password == '') {
//       return Alert.alert('Please fill all fileds');
//     }
//     let body={
//       firstName:FName.toLocaleLowerCase(),
//        lastName:LName.toLocaleLowerCase(),
//        email:email.toLocaleLowerCase() ,
//        password:password.toLocaleLowerCase(),
//        roleId: "5ff5a043c51e0042898fe15e"
//       }
//    await   console.log("body",body)


//    let res= await dispatch(Register(body))
//    if(res===true){
 
//     Alert.alert(
//      "",
//      "User Register Sucessfull",
//      [
//          { text: "Ok", onPress: () =>goToSignIn() },
//      ],
//      { cancelable: false },
//  );
//  return true;
//  } 
//  else{
//      setLoader(false)
//  }
//              }
  
// const login = async () => {
//   if (email == '' && password == '') {
//     return Alert.alert('Please fill all fileds');
//   }

//   // ============= POST API CALL BY REDUX ==============
//   let res = await dispatch(
//     loginData(email.toLocaleLowerCase(), password.toLocaleLowerCase()),
//   );
//   console.log("login ressss",res)
//   if (res == true) {
  
//     navigation.navigate('DrawerScreen');
//   } else {
//   }
// };

  return (
    <View>
      <ImageBackground
        source={PATH.backGroundImage}
        style={{height: '100%', width: '100%'}}>
        
        <Image source={PATH.logo}   resizeMode="contain"     style={styles.logo} />
        <TextInput
          placeHolder={'Email'}
          onChange={(email)=>setEmail(email)}
          ></TextInput>
        <TextInput
          placeHolder={'Password'}
          onChange={(password)=>setPassword(password)}
          ></TextInput>
        <TouchableOpacity onpress={()=>forgot()}>
          <Text style={styles.forgotText}>FORGOT PASSWORD?</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() =>  navigation.navigate('DrawerScreen')}>
          <View style={styles.buttonView}>
            <Text style={styles.submitText}>LOGIN</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
          <Text
            style={{
              color: 'white',
              alignSelf: 'center',
              marginTop: '10%',
              fontSize: 17,
            }}>
            NOT REGISTER YET? REGISTER
          </Text>
        </TouchableOpacity>
      </ImageBackground>
     
    </View>
  );
}
export default SignIn;

const styles = StyleSheet.create({
  logo: {
    height:hp("30%"),
    width: wp("20%"),
    alignSelf: 'center',
    marginTop: '7%',
  },
  emailInput: {
    marginLeft: '5%',
    color: 'white',
  },

  textInputView: {
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    width: '80%',
    flexDirection: 'row',
    marginLeft: '10%',
  },
  passwordView: {
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    width: '80%',
    flexDirection: 'row',
    marginLeft: '10%',
    marginTop: '10%',
  },
  emailSize: {
    height: 25,
    width: 25,
    marginTop: '3%',
  },
  forgotText: {
    color: 'white',
    marginLeft: '55%',
    marginTop: '5%',
  },
  buttonView: {
    height: 40,
    width: '80%',
    backgroundColor: 'white',
    marginLeft: '10%',
    marginTop: '7%',
    borderRadius: 30,
    justifyContent: 'center',
  },
  submitText: {
    color: 'green',
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  facebookView: {
    height: '6%',
    width: '80%',
    backgroundColor: '#03fcba',
    marginLeft: '10%',
    marginTop: '3%',
    borderRadius: 30,
    justifyContent: 'center',
  },
});
