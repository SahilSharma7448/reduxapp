import axios from 'axios';
import {Alert} from 'react-native';
import * as Services from '../../api/services';
import * as Url from '../../api/Url';
export const LOGIN_DATA = 'LOGIN_DATA';
export const REGISTER_DATA ='REGISTER_DATA'
export const SEARCH_DATA='SEARCH_DATA'


import * as Utility from '../../utility/index';
import {useDispatch, useSelector} from 'react-redux';



export const loginData = (email, password) => async (dispatch) => {
  let body = {
    email: email,
    password: password,
  };
  console.log('body', body);
  let res = await Services.post(Url.LOGIN, '', body);
  // console.log("res service",res)
  dispatch({
    type: LOGIN_DATA,
    payload: res,
  });
  if (res.isSuccess == true) {
    await Utility.setInLocalStorge('token', res.data.token);
    console.log("token",res.data.token)
    await Utility.setInLocalStorge('userData', res.data.firstName);
    await Utility.setInLocalStorge('userId', res.data.id);

    return true;
  }
  return;
};


export const create = (body) => async (dispatch) => {
  let res = await Services.post(Url.REGISTER, '', body);
  console.log('response', res);
  dispatch({
    type: REGISTER_DATA,
    payload: res,
  });
  if (res.isSuccess == true) {
    return true;
  }
  return;
};

export const search=(searchName)=>async(dispatch)=>{
  let token = await Utility.getFromLocalStorge('token')
  console.log("new token",token)
  console.log("searchName",searchName)
  let res =await Services.get(Url.SEARCH+ `searchString=${searchName}&sortByName=${searchName}&role=customer`,token);
  
  console.log('response',res);
  console.log("new data is ",res.data)

  
  dispatch({
    type:SEARCH_DATA,
    payload:res,
  });
  if(res.isSuccess == true)

{
  return true;

}
return;
};