import {DELETE_USER} from '../../api/Url';
import {
  FETCH_DATA,
  LOGIN_DATA,
  REGISTER_DATA,
  SEARCH_DATA,
 
} from './Action';
const initialState = {
  user: [],
  LoginData: {},
  RegisterData: {},
  SearchData:{},
  
};

////========== SET ALL DATA IN STORE BY UNIQUE STATE ========
export default function (state = initialState, {type, payload}) {
  switch (type) {
    case FETCH_DATA:
      return {
        ...state,
        user: payload,
      };
    case LOGIN_DATA: {
      return {
        ...state,
        LoginData: payload,
      };
    }
    case REGISTER_DATA:{
      return{
        ...state,
        RegisterData:payload,
      };
    }
    case SEARCH_DATA: {
      return{
        ...state,
        SearchData:payload
      }
    }
    default:
      return state;
  }
}
