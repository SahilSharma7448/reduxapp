import * as React from "react";
import {
  SafeAreaView, StyleSheet, Dimensions, View, Animated, Text,TouchableWithoutFeedback
} from "react-native";
import * as shape from "d3-shape";
import Svg, { Path } from 'react-native-svg';
import StaticTabbar from "../Components/Statictabbar";
import SIgnIN from "../screens/AuthScreens/SignIn"
const AnimatedSvg = Animated.createAnimatedComponent(Svg);
const { width } = Dimensions.get("window");
const height = 64;
// const { Path } = Svg;
const tabs = [
  {
    name: "grid",
  },
  {
    name: "list",
  },
  {
    name: "repeat",
  },
  {
    name: "map",
  },
  {
    name: "user",
  },
];
const tabWidth = width / tabs.length;
const backgroundColor = "white";

const getPath = (): string => {
  const left = shape.line().x(d => d.x).y(d => d.y)([
    { x: 0, y: 0 },
    { x: width, y: 0 },
  ]);
  const tab = shape.line().x(d => d.x).y(d => d.y).curve(shape.curveBasis)([
    { x: width, y: 0 },
    { x: width + 5, y: 0 },
    { x: width + 10, y: 10 },
    { x: width + 15, y: height },
    { x: width + tabWidth - 15, y: height },
    { x: width + tabWidth - 10, y: 10 },
    { x: width + tabWidth - 5, y: 0 },
    { x: width + tabWidth, y: 0 },
  ]);
  const right = shape.line().x(d => d.x).y(d => d.y)([
    { x: width + tabWidth, y: 0 },
    { x: width * 2, y: 0 },
    { x: width * 2, y: height },
    { x: 0, y: height },
    { x: 0, y: 0 },
  ]);
  return `${left} ${tab} ${right}`;
};
const d = getPath();
interface TabbarProps {}

// eslint-disable-next-line react/prefer-stateless-function
// export default class Tabbar extends React.PureComponent<TabbarProps> {
  const Tabbar =()=>{
  const  onPress = async(index: number) => {
      console.log("check ",index)
      // const { value, tabs } = this.props;
      const tabWidth = width / tabs.length;
      Animated.sequence([
        Animated.parallel(
          values.map(v => Animated.timing(v, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true,
          })),
        ),
        Animated.parallel([
          Animated.spring(value, {
            toValue: tabWidth * index,
            useNativeDriver: true,
          }),
          Animated.spring(values[index], {
            toValue: 1,
            useNativeDriver: true,
          }),
        ]),
      ]).start();
    }
const  values: Animated.Value[] = tabs.map((tab, index) => new Animated.Value(index === 0 ? 1 : 0));
  // render() {
    const value = new Animated.Value(0);;
    console.log("value",value)
    const translateX = value.interpolate({
      inputRange: [0, width],
      outputRange: [-width, 0],
    });
    return (
      <View style={{flex:1}}>
       <SIgnIN></SIgnIN>

      <View style={{  
      // backgroundColor: "#eb3345",
      position:"absolute",
      bottom:0,
      justifyContent: "flex-end",}}>

        <View {...{ height, width }}>
          <AnimatedSvg width={width * 2} {...{ height }} style={{ transform: [{ translateX }] }}>
            <Path fill={backgroundColor} {...{ d }} />
          </AnimatedSvg>
          <View style={StyleSheet.absoluteFill}>
            {/* <StaticTabbar {...{ tabs, value }} /> */}
            <View style={ {
        flexDirection: "row",
      }}>
        {
          tabs.map((tab, key) => {
            const tabWidth = width / tabs.length;
            const cursor = tabWidth * key;
            const opacity = value.interpolate({
              inputRange: [cursor - tabWidth, cursor, cursor + tabWidth],
              outputRange: [1, 0, 1],
              extrapolate: "clamp",
            });
            const translateY = values[key].interpolate({
              inputRange: [0, 1],
              outputRange: [64, 0],
              extrapolate: "clamp",
            });
            const opacity1 = values[key].interpolate({
              inputRange: [0, 1],
              outputRange: [0, 1],
              extrapolate: "clamp",
            });
            return (

              <React.Fragment {...{ key }}>
                <TouchableWithoutFeedback onPress={() => onPress(key)}>
                  <Animated.View style={[styles.tab, { opacity }]}>
                    <Text>sahil</Text>
                  </Animated.View>
                </TouchableWithoutFeedback>
                <Animated.View
                  style={{
                    position: "absolute",
                    top: -8,
                    left: tabWidth * key,
                    width: tabWidth,
                    height: 64,
                    justifyContent: "center",
                    alignItems: "center",
                    opacity: opacity1,
                    transform: [{ translateY }],
                  }}
                >
                  <View style={styles.activeIcon}>
                    <Text>amit</Text>
                  </View>
                </Animated.View>
              </React.Fragment>

            );
          })
        }
        </View>
          </View>
        </View>
        <SafeAreaView style={styles.container} />
      </View>
      </View>
    );
  }
export default Tabbar;
const styles = StyleSheet.create({
  container: {
    backgroundColor,
  },
  tab: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 64,
  },
  activeIcon: {
    backgroundColor: "white",
    width: 40,
    height: 40,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
  },
});
