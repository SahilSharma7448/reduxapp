
import React from "react"
import {NavigationContainer} from '@react-navigation/native';
import configureStore from './app/Redux/User/configureStore';
import Login from "./app/screens/AuthScreens/SignIn"
import { Provider } from 'react-redux';
// import Routes from "./app/navigation/index"
import {MainStackNavigator} from "./navigator/StackNavigator"
import BottomTabNavigator from "./navigator/TabNavigator";
import Bottom from "./app/Components/Tabbar"
const store = configureStore()
const App=()=>{
  return(
<Provider store={store}>
{/* /////////////////////GO TO NAVIGATOR FOLDER => STACK NAVIGATOR /////////// */}
 <NavigationContainer>
 <MainStackNavigator></MainStackNavigator>
    </NavigationContainer>
</Provider>
  )
}
export default App;

