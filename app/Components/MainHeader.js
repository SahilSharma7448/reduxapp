
import React ,{} from 'react'
import  {View,Text,ImageBackground,Image}from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import PATH from "../Constants/ImagePath"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../utility/index";

const  MainHeader=({navigation})=>{
    return(
        <View style={{background:"white",width:"100%",height:"7%",justifyContent:"center"}}>
             <TouchableOpacity  onPress={() => navigation.openDrawer()}>
<Image source={PATH.menu} style={{width:wp("5%"),height:hp("5%"),left:10}}></Image>

             </TouchableOpacity>
            </View>
        
    );

}
export default MainHeader;