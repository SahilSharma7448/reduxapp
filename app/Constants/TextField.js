import React, {useEffect, useState} from 'react';
import { Button, View,Text, Alert,StyleSheet,Image,TextInput } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { NavigationActions, StackActions } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../utility/index";

export default function TextField({onChange,placeHolder,userImg,style,security}) {

  return (
      <View >
        <View style={styles.textInputView}>
              <TextInput placeholder={placeHolder}
              placeholderTextColor="white"
              style={styles.emailInput}
              onChangeText={onChange}
              />
              </View>
   </View>
  );
}

const styles = StyleSheet.create({
  textInputView:{
    borderBottomWidth:1,
    borderBottomColor:"white",
     width:"80%",
     flexDirection:"row",
     marginLeft:"10%",
     marginTop:20
},
emailSize:{
  height:25,
  width:25,
  marginTop:"3%"
},
emailInput:{
  color:"white",
  marginBottom:10
},
})
